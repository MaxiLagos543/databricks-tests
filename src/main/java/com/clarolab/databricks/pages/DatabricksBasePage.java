package com.clarolab.databricks.pages;

import com.clarolab.selenium.pages.browser.web.WebBrowser;
import com.clarolab.selenium.pages.pages.BaseTopLevelPage;

public class DatabricksBasePage extends BaseTopLevelPage {

    public void setTestUrl(String url) {
        ((WebBrowser)a.getBrowser()).openPageByURL(url);
    }

}
