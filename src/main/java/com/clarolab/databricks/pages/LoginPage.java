package com.clarolab.databricks.pages;

import com.clarolab.selenium.pages.config.TimeoutType;
import org.openqa.selenium.By;

public class LoginPage extends DatabricksBasePage {

    private static final By MAIN_PAGE_ID = By.id("login-page");
    private static final By EMAIL_INPUT_ID = By.id("login-email");
    private static final By PASSWORD_INPUT_ID = By.className("login-password");
    private static final By SUBMIT_BUTTON_CLASS = By.className("signin");
    private static final By SWITCH_TO_ADMIN_LOGIN_BUTTON_CLASS = By.className("sso-tab-link");

    @Override
    public By getPageIdentifier() {
        return MAIN_PAGE_ID;
    }

    public void enterEmail(String email) {
        a.clearText(EMAIL_INPUT_ID);
        a.inputText(EMAIL_INPUT_ID, email);
    }

    public void enterPassword(String password) {
        a.clearText(PASSWORD_INPUT_ID);
        a.inputText(PASSWORD_INPUT_ID, password);
    }

    public HomePage clickSubmitButton() {
        return a.clickAndLoadTopLevelPage(SUBMIT_BUTTON_CLASS, HomePage.class, TimeoutType.PAGE_LOAD_TIMEOUT);
    }

    public void clickAdminLogIn() {
        a.click(SWITCH_TO_ADMIN_LOGIN_BUTTON_CLASS, TimeoutType.CLICK_TIMEOUT);
    }

}
