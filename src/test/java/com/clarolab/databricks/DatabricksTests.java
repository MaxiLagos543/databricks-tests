package com.clarolab.databricks;

import com.clarolab.databricks.pages.HomePage;
import com.clarolab.databricks.pages.LoginPage;
import com.clarolab.databricks.utils.PropertiesUtils;
import com.clarolab.selenium.pages.browser.LocalBrowserBuilder;
import com.clarolab.selenium.pages.browser.web.ChromeBrowser;
import com.clarolab.selenium.pages.browser.web.WebBrowser;
import com.clarolab.selenium.pages.exception.FrameworkWebDriverException;
import com.clarolab.selenium.pages.pages.SubPage;
import com.clarolab.selenium.pages.pages.TopLevelPage;
import org.apache.commons.exec.environment.EnvironmentUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class DatabricksTests {

    WebBrowser browser;

    protected WebBrowser createMinimalChrome() throws FrameworkWebDriverException {
        return LocalBrowserBuilder.getChromeBuilder(PropertiesUtils.url)
                .withWebDriverPath(PropertiesUtils.browserPath)
                .build();
    }

    @BeforeMethod(alwaysRun = true)
    protected void initTest() throws FrameworkWebDriverException {
        browser = createMinimalChrome();
        getBrowser().openPageByURL(PropertiesUtils.url);
    }

    @AfterMethod(alwaysRun = true)
    protected void cleanTest() {
        getBrowser().cleanSession();
        getBrowser().quit();
    }

    public  <T extends TopLevelPage> T loadTopPage(Class<T> pageClass){
        return getBrowser().loadTopLevelPage(pageClass);
    }

    public  <T extends SubPage> T loadSubPage(Class<T> pageClass){
        return getBrowser().loadSubPage(pageClass);
    }

    /*Login app is widely used*/
    public HomePage loginApp() {
        LoginPage loginPage = loadTopPage(LoginPage.class);
        loginPage.enterEmail(PropertiesUtils.username);
        loginPage.enterPassword(PropertiesUtils.password);
        return loginPage.clickSubmitButton();
    }

    /*Login app is widely used*/
    public HomePage loginAsAdminSSO() {
        LoginPage loginPage = loadTopPage(LoginPage.class);
        loginPage.clickAdminLogIn();
        loginPage.enterEmail(PropertiesUtils.username);
        loginPage.enterPassword(PropertiesUtils.password);
        return loginPage.clickSubmitButton();
    }

    /*LogOut app is widely used*/
    public void logOutApp() {
//        LoginPage loginPage = loadTopPage(LoginPage.class);
//        loginPage.logOutAppByURL();
    }

    protected WebBrowser getBrowser(){
        return browser;
    }

}
