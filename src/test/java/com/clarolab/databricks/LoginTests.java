package com.clarolab.databricks;

import com.clarolab.databricks.pages.DatabricksBasePage;
import org.testng.annotations.Test;

public class LoginTests extends DatabricksTests {

    @Test(groups = {"login"}, description = "Test Login app")
    public void login() {
        loginAsAdminSSO();
    }

}
